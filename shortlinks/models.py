from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models

from accounts.models import Account
from catsordogs.utils.random import random_string


def random_path_token() -> str:
    return random_string(
        alphabet=settings.SHORTLINK_PATH_TOKEN_ALPHABET, size=64
    )


class ShortLink(models.Model):
    dt_created = models.DateTimeField(auto_now_add=True)
    dt_updated = models.DateTimeField(auto_now=True)

    creator = models.ForeignKey(Account, on_delete=models.PROTECT)
    original_url = models.URLField()
    path_token = models.CharField(
        max_length=64,
        unique=True,
        db_index=True,
        validators=[RegexValidator()],
    )

    def __str__(self) -> str:
        return (
            f'{self.__class__.__name__}'
            f'(pk={self.pk}, path_token="{self.path_token}")'
        )
