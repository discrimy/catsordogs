from .redirect_from_short_link import redirect_from_short_link

__all__ = ['redirect_from_short_link']
