import logging

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from shortlinks.models import ShortLink


logger = logging.getLogger(__name__)


@wrap_exception(ShortLink.DoesNotExist, NotFoundError)
def redirect_from_short_link(path_token: str) -> str:
    short_link: ShortLink = ShortLink.objects.get(path_token=path_token)
    logger.info(
        f'Redirect from shortlink "{short_link}" to {short_link.original_url}'
    )
    return short_link.original_url
