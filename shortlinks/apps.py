from django.apps import AppConfig


class ShorlinksConfig(AppConfig):
    name = 'shortlinks'
