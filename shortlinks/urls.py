from django.urls import path

from shortlinks import views

app_name = 'shoirtlinks'
urlpatterns = [
    path('<str:path_token>/', views.redirect_from_short_link, name='redirect'),
]
