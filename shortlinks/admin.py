from django.contrib import admin

from shortlinks.models import ShortLink


@admin.register(ShortLink)
class ShortLinkAdmin(admin.ModelAdmin):
    model = ShortLink
