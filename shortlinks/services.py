from typing import Optional

from django.db import transaction, IntegrityError

from accounts.models import Account
from shortlinks.models import ShortLink, random_path_token


class CannotCreateShortLinkError(Exception):
    pass


@transaction.atomic()
def create_short_link(
    creator: Account,
    original_url: str,
    path_token: Optional[str] = None,
) -> ShortLink:
    if path_token is None:
        path_token = random_path_token()

    try:
        short_link = ShortLink.objects.create(
            creator=creator,
            original_url=original_url,
            path_token=path_token,
        )
    except IntegrityError as error:
        raise CannotCreateShortLinkError() from error
    else:
        return short_link
