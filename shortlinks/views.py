from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import redirect

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from shortlinks import use_cases


@wrap_exception(NotFoundError, Http404)
def redirect_from_short_link(
    request: HttpRequest, path_token: str
) -> HttpResponse:
    original_url = use_cases.redirect_from_short_link(
        path_token=path_token,
    )
    return redirect(original_url)
