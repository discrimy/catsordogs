from typing import Optional

from django.db import transaction

from notifications import services as notifications_services
from polls import services as polls_services

from accounts.models import Account
from polls.models import Poll
from polls.use_cases.utils import get_poll


@transaction.atomic()
def create_comment(
    poll_pk: Optional[int],
    secret_token: Optional[str],
    author_pk: int,
    content: str,
) -> str:
    poll = get_poll(
        base_queryset=Poll.objects.all(),
        poll_pk=poll_pk,
        secret_token=secret_token,
    )
    author = Account.objects.get(pk=author_pk)
    comment = polls_services.add_comment_to_poll(poll, content, author)
    if poll.author != author:
        notifications_services.send_notification_poll_new_comment(
            to_account=poll.author, comment=comment
        )
    return poll.url_for_view
