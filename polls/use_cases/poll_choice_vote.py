from typing import Optional

from django.db import transaction

from polls import services as polls_services

from accounts.models import Account
from polls.models import Poll
from polls.use_cases.utils import get_poll

PollURL = str


@transaction.atomic()
def vote_for_choice(
    poll_pk: Optional[int],
    secret_token: Optional[str],
    choice_pk: int,
    by_account_pk: int,
) -> Poll:
    account = Account.objects.get(pk=by_account_pk)
    poll = get_poll(
        Poll.objects.prefetch_related('choices'), poll_pk, secret_token
    )
    choice = poll.choices.get(pk=choice_pk)
    polls_services.vote_for_choice(choice, account)
    poll.save()
    return poll
