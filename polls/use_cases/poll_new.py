import datetime as dt
from typing import BinaryIO, List, Optional

from django.core.files import File
from django.db import transaction

from polls import services as polls_services

from accounts.models import Account
from polls.models import ConfidentialityType

CreatedPollURL = str


@transaction.atomic()
def create_poll(
    title: str,
    description: str,
    author_pk: int,
    choices_titles: List[str],
    allow_multiple_votes: bool,
    allow_undo_votes: bool,
    confidentiality_type: ConfidentialityType,
    background_image: Optional[BinaryIO],
    dt_close_after: Optional[dt.datetime],
    max_voters_count: Optional[int],
    is_public: bool,
) -> CreatedPollURL:
    author = Account.objects.get(pk=author_pk)
    poll = polls_services.create_poll(
        title=title,
        description=description,
        author=author,
        choice_titles=choices_titles,
        multiple_votes=allow_multiple_votes,
        can_undo_votes=allow_undo_votes,
        confidentiality_type=confidentiality_type,
        background_image=(
            File(background_image) if background_image else None
        ),
        dt_close_after=dt_close_after,
        voters_count_close=max_voters_count,
        is_public=is_public,
    )
    return poll.url_for_view
