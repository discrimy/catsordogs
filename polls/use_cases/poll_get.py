from dataclasses import dataclass
from typing import List, Optional

from accounts.models import Account
from polls.models import ConfidentialityType, Poll
from polls.use_cases.utils import get_poll


@dataclass(frozen=True)
class VoterSchema:
    username: str


@dataclass(frozen=True)
class ChoiceSchema:
    title: str
    url_for_vote: str
    url_for_unvote: str
    voters: List[VoterSchema]
    can_be_voted: bool
    can_be_unvoted: bool


@dataclass(frozen=True)
class CommentSchema:
    pk: int
    author_username: str
    content: str


@dataclass(frozen=True)
class PollViewerSpecificSchema:
    background_image_url: Optional[str]
    background_image_filename: Optional[str]
    is_closed: bool
    show_voters: bool
    author_username: str
    description: str
    choices: List[ChoiceSchema]
    comments: List[CommentSchema]
    url_for_comment: str

    @classmethod
    def from_poll(
        cls, poll: Poll, current_account: Optional[Account]
    ) -> 'PollViewerSpecificSchema':
        return PollViewerSpecificSchema(
            background_image_url=(
                poll.background_image.url if poll.background_image else None
            ),
            background_image_filename=(
                poll.background_image.name if poll.background_image else None
            ),
            is_closed=poll.is_closed,
            show_voters=poll.confidentiality_type == ConfidentialityType.OPEN,
            author_username=poll.author.user.username,
            description=poll.description,
            choices=[
                ChoiceSchema(
                    title=choice.title,
                    url_for_vote=choice.url_for_vote,
                    url_for_unvote=choice.url_for_unvote,
                    voters=[
                        VoterSchema(username=voter.user.username)
                        for voter in choice.voters.all()
                    ],
                    can_be_voted=(
                        choice.can_be_voted_by(current_account)
                        if current_account is not None
                        else False
                    ),
                    can_be_unvoted=(
                        choice.can_be_unvoted_by(current_account)
                        if current_account is not None
                        else False
                    ),
                )
                for choice in poll.choices.all()
            ],
            comments=[
                CommentSchema(
                    pk=comment.pk,
                    author_username=comment.author.user.username,
                    content=comment.content,
                )
                for comment in poll.comments.all()
            ],
            url_for_comment=poll.url_for_comment,
        )


def get_poll_details(
    poll_pk: Optional[int],
    secret_token: Optional[str],
    account_pk: Optional[int],
) -> PollViewerSpecificSchema:
    poll = get_poll(
        base_queryset=Poll.objects.select_related('author').prefetch_related(
            'choices',
            'choices__voters',
            'choices__voters__user',
            'comments',
            'comments__author',
            'comments__author__user',
        ),
        secret_token=secret_token,
        poll_pk=poll_pk,
    )

    current_account: Optional[Account]
    if account_pk:
        current_account = Account.objects.get(pk=account_pk)
    else:
        current_account = None

    return PollViewerSpecificSchema.from_poll(
        poll, current_account=current_account
    )
