from typing import Optional

from django.db.models import QuerySet

from polls.models import Poll


def get_poll(
    base_queryset: QuerySet[Poll],
    poll_pk: Optional[str],
    secret_token: Optional[str],
) -> Poll:
    if poll_pk is not None and secret_token is None:
        return base_queryset.public_only().get(pk=poll_pk)
    if poll_pk is None and secret_token is not None:
        return base_queryset.get(secret_token=secret_token)
    else:
        raise ValueError('Call with either poll pk or secret token')
