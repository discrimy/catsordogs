from dataclasses import dataclass
from typing import List

from polls.models import Poll


@dataclass(frozen=True)
class PollSchema:
    id: int
    title: str

    @classmethod
    def from_poll(cls, poll: Poll) -> 'PollSchema':
        return PollSchema(id=poll.id, title=poll.title)


def get_public_polls() -> List[PollSchema]:
    return [PollSchema.from_poll(poll) for poll in Poll.objects.public_only()]
