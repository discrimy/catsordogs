from xml.etree.ElementTree import ElementTree

from django import template
from django.template.defaultfilters import stringfilter
from markdown import Markdown, markdown
from markdown.extensions import Extension
from markdown.treeprocessors import Treeprocessor

register = template.Library()


class ResponsiveImagesTreeprocessor(Treeprocessor):
    def run(self, root: ElementTree):
        for img_tag in root.iter('img'):
            img_tag.set('class', 'img-fluid')


class ResponsiveImagesExtension(Extension):
    def extendMarkdown(self, md: Markdown):
        md.treeprocessors.register(
            ResponsiveImagesTreeprocessor(md), 'responsive-images', 1
        )


@register.filter()
@stringfilter
def render_as_markdown(markdown_str: str):
    return markdown(
        markdown_str,
        extensions=[
            'markdown.extensions.fenced_code',
            ResponsiveImagesExtension(),
        ],
    )
