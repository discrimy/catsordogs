from pathlib import Path

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import QuerySet
from django.urls import reverse
from django.utils import timezone

from accounts.models import Account
from catsordogs.utils.models import PrintableModelMixin
from polls.secret_token_utils import generate_secret_token


class ConfidentialityType(models.TextChoices):
    OPEN = 'open', 'Open'
    SECRET = 'secret', 'Secret'


def upload_poll_background_to(poll: 'Poll', filename: str) -> Path:
    return Path(
        'polls',
        'backgrounds',
        generate_secret_token(size=32) + Path(filename).suffix,
    )


def is_secret_token(value: str):
    if value == '' or not all(
        character in settings.POLL_SECRET_KEY_ALPHABET for character in value
    ):
        raise ValidationError(f'Value {value} is not valid secret token')


class PollQuerySet(QuerySet):
    def public_only(self):
        return self.filter(secret_token=None)


class Poll(PrintableModelMixin, models.Model):
    objects = PollQuerySet.as_manager()

    author = models.ForeignKey(
        Account, on_delete=models.PROTECT, related_name='polls'
    )
    title = models.CharField(max_length=128)
    description = models.TextField(verbose_name='Description', blank=True)
    confidentiality_type = models.CharField(
        verbose_name='Confidentiality',
        max_length=32,
        choices=ConfidentialityType.choices,
        default='open',
    )
    multiple_votes = models.BooleanField(
        verbose_name='Multiple votes by one voter', default=False
    )
    can_undo_votes = models.BooleanField(
        verbose_name='Undo votes',
        default=False,
    )
    background_image = models.ImageField(
        verbose_name='Background image',
        upload_to=upload_poll_background_to,
        blank=True,
    )
    is_closed = models.BooleanField(verbose_name='Is closed', default=False)
    secret_token = models.CharField(
        verbose_name='Secret url slug',
        max_length=64,
        validators=[is_secret_token],
        blank=True,
        null=True,
        unique=True,
    )

    def voted_choices_by(self, account: Account) -> 'QuerySet[Choice]':
        return self.choices.filter(voters=account)

    @property
    def is_public(self) -> bool:
        return self.secret_token is None

    @property
    def url_for_view(self) -> str:
        if self.is_public:
            return reverse('polls:polls_details', kwargs={'poll_pk': self.pk})
        else:
            return reverse(
                'polls:secret_polls_details',
                kwargs={'secret_token': self.secret_token},
            )

    @property
    def url_for_comment(self) -> str:
        if self.is_public:
            return reverse('polls:polls_comment', kwargs={'poll_pk': self.pk})
        else:
            return reverse(
                'polls:secret_polls_comment',
                kwargs={'secret_token': self.secret_token},
            )

    def get_absolute_url(self) -> str:
        return self.url_for_view


class PollClosingByTime(PrintableModelMixin, models.Model):
    poll = models.OneToOneField(
        Poll, on_delete=models.CASCADE, related_name='closing_by_time'
    )
    dt_close_after = models.DateTimeField(verbose_name='Close after')

    @property
    def triggered(self):
        return timezone.now() > self.dt_close_after


class PollClosingByVoters(PrintableModelMixin, models.Model):
    poll = models.OneToOneField(
        Poll, on_delete=models.CASCADE, related_name='closing_by_voters'
    )
    count = models.IntegerField(verbose_name='Count of voters')

    @property
    def triggered(self):
        return (
            Account.objects.filter(choices__poll=self.poll).count()
            >= self.count
        )


class PollClosingManual(PrintableModelMixin, models.Model):
    poll = models.OneToOneField(
        Poll, on_delete=models.CASCADE, related_name='closing_manual'
    )
    account_closed = models.ForeignKey(
        Account, on_delete=models.PROTECT, related_name='poll_closings'
    )

    @property
    def triggered(self):
        return True


class Choice(PrintableModelMixin, models.Model):
    title = models.CharField(max_length=128)
    poll = models.ForeignKey(
        Poll, related_name='choices', on_delete=models.CASCADE
    )
    voters = models.ManyToManyField(
        Account, related_name='choices', blank=True
    )

    def is_voted_by(self, account: Account) -> bool:
        return self.voters.filter(pk=account.id).exists()

    def can_be_voted_by(self, account: Account) -> bool:
        if self.poll.is_closed:
            return False
        if self.poll.multiple_votes:
            if self.is_voted_by(account):
                return False
        else:
            if self.poll.voted_choices_by(account).exists():
                return False
        return True

    def can_be_unvoted_by(self, account: Account) -> bool:
        if self.poll.is_closed:
            return False
        if not self.poll.can_undo_votes:
            return False
        return self.is_voted_by(account)

    @property
    def url_for_vote(self):
        if self.poll.is_public:
            return reverse(
                'polls:polls_vote',
                kwargs={'choice_pk': self.pk, 'poll_pk': self.poll.pk},
            )
        else:
            return reverse(
                'polls:secret_polls_vote',
                kwargs={
                    'choice_pk': self.pk,
                    'secret_token': self.poll.secret_token,
                },
            )

    @property
    def url_for_unvote(self):
        if self.poll.is_public:
            return reverse(
                'polls:polls_unvote',
                kwargs={'choice_pk': self.pk, 'poll_pk': self.poll.pk},
            )
        else:
            return reverse(
                'polls:secret_polls_unvote',
                kwargs={
                    'choice_pk': self.pk,
                    'secret_token': self.poll.secret_token,
                },
            )


class Comment(PrintableModelMixin, models.Model):
    author = models.ForeignKey(Account, on_delete=models.PROTECT)
    poll = models.ForeignKey(
        Poll, on_delete=models.CASCADE, related_name='comments'
    )
    content = models.TextField(max_length=256)
