from django.contrib import admin

from polls import models


class ChoiceInline(admin.TabularInline):
    model = models.Choice
    extra = 1


class PollClosingByTimeInline(admin.StackedInline):
    model = models.PollClosingByTime


class PollClosingByVotersInline(admin.StackedInline):
    model = models.PollClosingByVoters


class PollClosingManualInline(admin.StackedInline):
    model = models.PollClosingManual


class CommentInline(admin.StackedInline):
    model = models.Comment
    extra = 1


@admin.register(models.Poll)
class PollsAdmin(admin.ModelAdmin):
    inlines = [
        PollClosingByTimeInline,
        PollClosingByVotersInline,
        PollClosingManualInline,
        ChoiceInline,
        CommentInline,
    ]
    readonly_fields = [
        'confidentiality_type',
        'multiple_votes',
    ]
