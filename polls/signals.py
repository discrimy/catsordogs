from logging import getLogger
from typing import Type

from django.db.models.signals import pre_save
from django.dispatch import receiver

from polls.models import Poll


logger = getLogger(__name__)


@receiver(pre_save, sender=Poll)
def update_poll_is_closed(
    sender: Type[Poll],
    instance: Poll,
    **kwargs,
) -> None:
    if instance.is_closed:
        return

    closed_by_time = (
        hasattr(instance, 'closing_by_time')
        and instance.closing_by_time.triggered
    )
    closed_by_voters = (
        hasattr(instance, 'closing_by_voters')
        and instance.closing_by_voters.triggered
    )
    closed_manual = (
        hasattr(instance, 'closing_manual')
        and instance.closing_manual.triggered
    )
    if closed_by_time or closed_by_voters or closed_manual:
        logger.info(f'Poll {instance} is closed now')
        instance.is_closed = True
