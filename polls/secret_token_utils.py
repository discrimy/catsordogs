from django.conf import settings

from catsordogs.utils.random import random_string


def generate_secret_token(size: int) -> str:
    return random_string(alphabet=settings.POLL_SECRET_KEY_ALPHABET, size=size)
