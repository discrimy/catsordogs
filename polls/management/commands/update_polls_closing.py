from django.core.management import BaseCommand
from django.db import transaction

from polls.models import Poll


class Command(BaseCommand):
    def handle(self, *args, **options):
        with transaction.atomic():
            poll: Poll
            for poll in Poll.objects.filter(is_closed=False).iterator(
                chunk_size=100
            ):
                poll.update_is_closed()
                poll.save()
