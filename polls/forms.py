from django import forms
from django.core.exceptions import ValidationError
from django.forms import inlineformset_factory
from django.utils import timezone

from polls.models import Choice, Comment, Poll


class PollForm(forms.ModelForm):
    dt_close_after = forms.DateTimeField(
        widget=forms.SelectDateWidget,
        required=False,
        help_text='When the poll must close. Leave empty if no needed.',
    )
    voters_count_close = forms.IntegerField(
        required=False,
        help_text='Max count of voters. Leave empty if no needed.',
    )
    is_public = forms.BooleanField(
        label='Public',
        required=False,
        help_text='Should this poll be accessible by random url.',
        initial=True,
    )

    class Meta:
        model = Poll
        fields = [
            'title',
            'description',
            'multiple_votes',
            'can_undo_votes',
            'confidentiality_type',
            'dt_close_after',
            'voters_count_close',
            'background_image',
            'is_public',
        ]

    def clean_dt_close_after(self):
        dt_close_after = self.cleaned_data['dt_close_after']
        if dt_close_after is not None and dt_close_after <= timezone.now():
            raise ValidationError('Close after date must be a future')
        return dt_close_after

    def clean_voters_count_close(self):
        voters_count_close = self.cleaned_data['voters_count_close']
        if voters_count_close is not None and voters_count_close < 1:
            raise ValidationError('Max voters count must be at least 1')
        return voters_count_close


ChoiceFormset = inlineformset_factory(
    Poll,
    Choice,
    fields=['title'],
    can_delete=False,
    min_num=1,
    validate_min=True,
)


class CommentNewForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']
