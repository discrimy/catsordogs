# Generated by Django 3.1.2 on 2020-11-01 16:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0013_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poll',
            name='multiple_votes',
            field=models.BooleanField(default=False, verbose_name='Multiple votes by one voter'),
        ),
    ]
