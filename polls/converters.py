from typing import Type, TypeVar

from django.conf import settings
from typing_extensions import Protocol

from django.urls.converters import StringConverter

T = TypeVar('T')


class ConverterProtocol(Protocol[T]):
    def to_python(self, value: str) -> T:
        ...

    def to_url(self, value: T) -> str:
        ...


def secret_converter_factory(
    alphabet: str = settings.POLL_SECRET_KEY_ALPHABET,
) -> Type[ConverterProtocol[str]]:
    class SecretConverter(StringConverter):
        regex = f'[{alphabet}]+'

    return SecretConverter
