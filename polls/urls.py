from django.urls import path, register_converter

from polls.converters import secret_converter_factory
from polls import views


register_converter(secret_converter_factory(), 'secret')


app_name = 'polls'
urlpatterns = [
    path('', views.poll_list, name='polls_list'),
    path('new/', views.poll_new, name='polls_new'),
    path(
        '<int:poll_pk>/',
        views.poll_description,
        name='polls_details',
    ),
    path(
        'secret/<secret:secret_token>/',
        views.poll_description,
        name='secret_polls_details',
    ),
    path(
        '<int:poll_pk>/vote/<int:choice_pk>/',
        views.poll_choice_vote,
        name='polls_vote',
    ),
    path(
        'secret/<secret:secret_token>/vote/<int:choice_pk>/',
        views.poll_choice_vote,
        name='secret_polls_vote',
    ),
    path(
        '<int:poll_pk>/unvote/<int:choice_pk>/',
        views.poll_choice_unvote,
        name='polls_unvote',
    ),
    path(
        'secret/<secret:secret_token>/unvote/<int:choice_pk>/',
        views.poll_choice_unvote,
        name='secret_polls_unvote',
    ),
    path(
        '<int:poll_pk>/comment/',
        views.poll_comment_new,
        name='polls_comment',
    ),
    path(
        'secret/<secret:secret_token>/comment/',
        views.poll_comment_new,
        name='secret_polls_comment',
    ),
]
