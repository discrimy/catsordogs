from typing import Optional

from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import render

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from polls import use_cases
from polls.forms import CommentNewForm


@wrap_exception(NotFoundError, Http404)
def poll_description(
    request: HttpRequest,
    poll_pk: Optional[int] = None,
    secret_token: Optional[str] = None,
) -> HttpResponse:
    if request.user.is_authenticated:
        account_pk = request.user.account.pk
    else:
        account_pk = None

    return render(
        request,
        'polls/poll.html',
        {
            'poll': use_cases.get_poll_details(
                poll_pk=poll_pk,
                secret_token=secret_token,
                account_pk=account_pk,
            ),
            'comment_form': CommentNewForm(),
        },
    )
