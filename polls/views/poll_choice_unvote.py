from typing import Optional, cast

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import redirect

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from polls import use_cases


@login_required()
@wrap_exception(NotFoundError, Http404)
def poll_choice_unvote(
    request: HttpRequest,
    choice_pk: int,
    poll_pk: Optional[int] = None,
    secret_token: Optional[str] = None,
) -> HttpResponse:
    account_pk = cast(User, request.user).account.pk

    poll = use_cases.unvote_for_choice(
        poll_pk,
        secret_token,
        choice_pk,
        by_account_pk=account_pk,
    )
    return redirect(poll.url_for_view)
