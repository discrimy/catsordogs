from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import render

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from polls import use_cases


@wrap_exception(NotFoundError, Http404)
def poll_list(
    request: HttpRequest,
) -> HttpResponse:
    return render(
        request,
        'polls/list.html',
        dict(
            polls=use_cases.get_public_polls(),
        ),
    )
