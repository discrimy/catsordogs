from typing import cast, Optional

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import redirect
from django.urls import reverse

from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception
from polls import use_cases
from polls.forms import CommentNewForm


@login_required()
@wrap_exception(NotFoundError, Http404)
def poll_comment_new(
    request: HttpRequest,
    poll_pk: Optional[int] = None,
    secret_token: Optional[str] = None,
) -> HttpResponse:
    form = CommentNewForm(request.POST)
    if form.is_valid():
        author_pk = cast(User, request.user).account.pk
        poll_url = use_cases.create_comment(
            poll_pk=poll_pk,
            secret_token=secret_token,
            author_pk=author_pk,
            content=form.cleaned_data['content'],
        )
        return redirect(poll_url)
    return redirect(reverse('polls:polls_new'))
