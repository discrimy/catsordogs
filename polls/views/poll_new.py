from typing import cast

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import (
    HttpRequest,
    HttpResponse,
    Http404,
)
from django.shortcuts import redirect, render

from catsordogs.utils.errors import wrap_exception
from catsordogs.utils.views import view_dispatch
from polls import use_cases
from polls.forms import ChoiceFormset, PollForm


def poll_new_get(request: HttpRequest) -> HttpResponse:
    return render(
        request,
        'polls/new.html',
        context=dict(poll_form=PollForm(), choices_formset=ChoiceFormset()),
    )


@login_required()
@wrap_exception(ValueError, Http404)
def poll_new_post(request: HttpRequest) -> HttpResponse:
    poll_form = PollForm(request.POST, request.FILES)
    choices_formset = ChoiceFormset(request.POST)
    if poll_form.is_valid() and choices_formset.is_valid():  # type: ignore
        author = cast(User, request.user).account
        choices_titles = [
            choice_form.cleaned_data['title']
            for choice_form in choices_formset
            if choice_form.cleaned_data
        ]
        created_poll_url = use_cases.create_poll(
            title=poll_form.cleaned_data['title'],
            description=poll_form.cleaned_data['description'],
            author_pk=author.pk,
            choices_titles=choices_titles,
            allow_multiple_votes=poll_form.cleaned_data['multiple_votes'],
            allow_undo_votes=poll_form.cleaned_data['can_undo_votes'],
            confidentiality_type=poll_form.cleaned_data[
                'confidentiality_type'
            ],
            background_image=poll_form.cleaned_data['background_image'],
            dt_close_after=poll_form.cleaned_data['dt_close_after'],
            max_voters_count=poll_form.cleaned_data['voters_count_close'],
            is_public=poll_form.cleaned_data['is_public'],
        )
        return redirect(created_poll_url)
    else:
        return redirect('polls:polls_new')


poll_new = view_dispatch(
    get_view=poll_new_get,
    post_view=poll_new_post,
)
