import logging
import re
from datetime import datetime
from typing import List, Optional, Union

from django.contrib.auth.models import User
from django.core.files import File
from django.db import transaction
from django.urls import reverse
from django.utils import timezone

from accounts.models import Account
from polls.exceptions import PollError
from polls.models import (
    Choice,
    Comment,
    ConfidentialityType,
    Poll,
    PollClosingByTime,
    PollClosingByVoters,
)
from polls.secret_token_utils import generate_secret_token
from shortlinks.services import create_short_link

logger = logging.getLogger(__name__)


_URL_REGEX = re.compile(
    r'https?://(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}'
    r'\.[a-zA-Z0-9]{1,6}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)'
)


def _replace_with_short_link(poll_author: Account, match: re.Match) -> str:
    short_link = create_short_link(
        creator=poll_author,
        original_url=match.group(),
    )
    return reverse(
        'shortlinks:redirect', kwargs=dict(path_token=short_link.path_token)
    )


def _parse_poll_description(poll_author: Account, description: str) -> str:
    return _URL_REGEX.sub(
        lambda match: _replace_with_short_link(
            poll_author=poll_author,
            match=match,
        ),
        description,
    )


@transaction.atomic()
def create_poll(
    title: str,
    description: str,
    author: Account,
    choice_titles: List[str],
    *,
    multiple_votes: bool = False,
    can_undo_votes: bool = False,
    confidentiality_type: ConfidentialityType = ConfidentialityType.OPEN,
    background_image: Optional[File] = None,
    dt_close_after: Optional[datetime] = None,
    voters_count_close: Optional[int] = None,
    is_public: bool = True,
) -> Poll:
    """
    Generic service method to create poll

    :param title: title of poll
    :param description: description of poll
    :param author: author of poll
    :param choice_titles: titles of choices of poll
    :param multiple_votes: do multiple votes from one voter allowed
    :param can_undo_votes: can voter undo vote
    :param confidentiality_type: show or hide voters of choice
    :param background_image: background image (banner)
    :param dt_close_after: close after this datetime
        (must be a future, ValueError will be raised otherwise)
    :param voters_count_close: max count of voters,
        after last vote poll will be closed
        (must be positive, ValueError will be raised otherwise)
    :param is_public: can poll be visible in public polls on main page
    :return: created Poll instance
    :raise ValueError: invalid value of any argument
    (see exception message)
    """
    if len(choice_titles) < 1:
        raise ValueError('There are must be at least 1 choice')
    if voters_count_close is not None and voters_count_close < 1:
        raise ValueError('Voters count must be > 1 or None')
    if dt_close_after is not None and dt_close_after <= timezone.now():
        raise ValueError('Close after must be a future or None')

    secret_token: Optional[str]
    if is_public:
        secret_token = generate_secret_token(size=64)
    else:
        secret_token = None

    parsed_description = _parse_poll_description(
        poll_author=author,
        description=description,
    )

    poll: Poll = Poll.objects.create(
        title=title,
        description=parsed_description,
        author=author,
        multiple_votes=multiple_votes,
        can_undo_votes=can_undo_votes,
        confidentiality_type=confidentiality_type,
        background_image=background_image,
        secret_token=secret_token,
    )
    for choice_title in choice_titles:
        poll.choices.add(Choice.objects.create(poll=poll, title=choice_title))
    if dt_close_after is not None:
        poll.closing_by_time = PollClosingByTime.objects.create(
            poll=poll, dt_close_after=dt_close_after
        )
    if voters_count_close is not None:
        poll.closing_by_voters = PollClosingByVoters.objects.create(
            poll=poll,
            count=voters_count_close,
        )
    return poll


@transaction.atomic()
def add_comment_to_poll(
    poll: Poll, comment_content: str, author: Union[User, Account]
) -> Comment:
    """
    Add comment to specific poll (poll will be updated too)

    :param poll: poll to comment
    :param comment_content: comment text
    :param author: comment author
    :return: created Comment instance
    """
    if isinstance(author, User):
        raise TypeError(
            f'Author must be Account instance, '
            f'not {author.__class__.__qualname__}'
        )

    comment = Comment.objects.create(
        poll=poll, content=comment_content, author=author
    )
    poll.comments.add(comment)
    return comment


@transaction.atomic()
def vote_for_choice(choice: Choice, voter: Union[User, Account]) -> None:
    """
    Vote for choice with voter

    :param choice: choice to vote
    :param voter: voter of choice
    :return: None
    :raise PollError: User cannot vote for choice (see exception message)
    """
    if isinstance(voter, User):
        raise TypeError(
            f'Voter must be Account instance, '
            f'not {voter.__class__.__qualname__}'
        )

    if not choice.can_be_voted_by(voter):
        raise PollError(f'User {voter} cannot vote for choice {choice}')
    choice.voters.add(voter)


@transaction.atomic()
def unvote_for_choice(choice: Choice, voter: Union[User, Account]) -> None:
    """
    Unvote for choice with voter

    :param choice: choice to vote
    :param voter: voter of choice
    :return: None
    :raise PollError: User cannot unvote for choice (see exception message)
    """
    if isinstance(voter, User):
        raise TypeError(
            f'Voter must be Account instance, '
            f'not {voter.__class__.__qualname__}'
        )

    if not choice.can_be_unvoted_by(voter):
        raise PollError(f'User {voter} cannot unvote for choice {choice}')
    choice.voters.remove(voter)
