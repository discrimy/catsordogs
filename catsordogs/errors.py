class ClientError(Exception):
    pass


class NotFoundError(ClientError):
    def __init__(self):
        super(NotFoundError, self).__init__('Queried resource is not found')


class ServerError(Exception):
    pass
