import string

import environ
from django.urls import reverse_lazy

root = environ.Path(__file__) - 2  # get root of the project
env = environ.Env(
    ALLOWED_HOSTS=(list, []),
)
environ.Env.read_env(env_file='.env')  # reading .env file

SITE_ROOT = root()
DEBUG = env.bool('DEBUG', default=False)
TEMPLATE_DEBUG = DEBUG

DATABASES = {'default': env.db('DATABASE_URL')}

public_root = root.path('public/')

# Choose file storage
if not DEBUG:
    # AWS S3
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

    AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = env.str('AWS_STORAGE_BUCKET_NAME')
    AWS_S3_REGION_NAME = env.str('AWS_S3_REGION_NAME')
    AWS_S3_ENDPOINT_URL = env.str('AWS_S3_ENDPOINT_URL')
else:
    DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'


MEDIA_ROOT = public_root('media')
MEDIA_URL = env.str('MEDIA_URL', default='media/')

STATIC_ROOT = public_root('static')
STATIC_URL = env.str('STATIC_URL', default='static/')
STATICFILES_DIRS = [
    root('static'),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

SECRET_KEY = env.str('SECRET_KEY')

POLL_SECRET_KEY_ALPHABET = (
    string.ascii_uppercase + string.ascii_lowercase + string.digits
)
SHORTLINK_PATH_TOKEN_ALPHABET = (
    string.ascii_uppercase + string.ascii_lowercase + string.digits
)

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS')


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'social_django',
    'bootstrap4',
    'debug_toolbar',
    'accounts',
    'notifications',
    'polls',
    'shortlinks',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'catsordogs.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'notifications.context_processors.user_notifications_count',
            ],
        },
    },
]

WSGI_APPLICATION = 'catsordogs.wsgi.application'

# Authentication

LOGIN_REDIRECT_URL = reverse_lazy('polls:polls_list')
LOGIN_URL = reverse_lazy('accounts:login')
LOGOUT_REDIRECT_URL = reverse_lazy('accounts:login')

AUTHENTICATION_BACKENDS = (
    'social_core.backends.vk.VKOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)
SOCIAL_AUTH_VK_OAUTH2_KEY = env.str('SOCIAL_AUTH_VK_OAUTH2_KEY', default='')
SOCIAL_AUTH_VK_OAUTH2_SECRET = env.str(
    'SOCIAL_AUTH_VK_OAUTH2_SECRET', default=''
)

INTERNAL_IPS = [
    '127.0.0.1',
]

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.'
        'UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
        'MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
        'CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
        'NumericPasswordValidator',
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {'format': '%(name)-12s %(levelname)-8s %(message)s'},
    },
    'handlers': {
        'console': {'class': 'logging.StreamHandler', 'formatter': 'console'},
    },
    'loggers': {
        '': {'level': 'INFO', 'handlers': ['console']},
        'django': {'level': 'WARNING', 'handlers': ['console']},
    },
}

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = env.str('TIME_ZONE', 'UTC')

USE_I18N = True

USE_L10N = True

USE_TZ = True
