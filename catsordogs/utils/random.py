import random


def random_string(alphabet: str, size: int) -> str:
    return ''.join(random.choices(alphabet, k=size))
