import functools
from typing import TypeVar, Type, Callable

RT = TypeVar('RT')


def wrap_exception(
    input_exception_type: Type[Exception],
    output_exception_type: Type[Exception],
) -> Callable[[Callable[..., RT]], Callable[..., RT]]:
    def decorator(func: Callable[..., RT]) -> Callable[..., RT]:
        @functools.wraps(func)
        def wrapper(*args, **kwargs) -> RT:
            try:
                return func(*args, **kwargs)
            except input_exception_type as error:
                raise output_exception_type() from error

        return wrapper

    return decorator
