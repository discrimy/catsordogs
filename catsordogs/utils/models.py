class PrintableModelMixin:
    pk: int

    def __str__(self) -> str:
        return f'<{self.__class__.__name__}#{self.pk}>'
