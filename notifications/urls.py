from django.urls import path

from notifications.views.notification_mark_as_read import (
    mark_notifications_as_read,
)

app_name = 'notifications'

urlpatterns = [
    path(
        'me/notifications/mark-as-read/',
        mark_notifications_as_read,
        name='notifications-mark-as-read',
    ),
]
