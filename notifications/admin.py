from django.contrib import admin

from notifications.models import Notification
from notifications.models import NotificationInfoPollNewComment


class NotificationInfoNewCommentInline(admin.StackedInline):
    model = NotificationInfoPollNewComment


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    inlines = [NotificationInfoNewCommentInline]
    readonly_fields = [
        'read',
    ]
