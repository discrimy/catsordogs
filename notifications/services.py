from django.db import transaction

from accounts.models import Account
from notifications.models import (
    Notification,
    NotificationInfoPollNewComment,
)
from polls.models import Comment


@transaction.atomic()
def send_notification_poll_new_comment(
    to_account: Account, comment: Comment
) -> Notification:
    notification_info = NotificationInfoPollNewComment.objects.create(
        notification=Notification.objects.create(
            to_account=to_account,
            read=False,
        ),
        comment=comment,
    )
    return notification_info.notification


@transaction.atomic()
def mark_all_notifications_as_read(of_account: Account) -> None:
    Notification.objects.filter(to_account=of_account, read=False).update(
        read=True
    )
