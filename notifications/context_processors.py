from typing import Dict, Optional

from django.http import HttpRequest

from notifications.models import Notification

USER_NOTIFICATIONS_COUNT_KEY = 'user_notifications_count'


def user_notifications_count(
    request: HttpRequest,
) -> Dict[str, Optional[int]]:
    if not request.user.is_authenticated:
        return {USER_NOTIFICATIONS_COUNT_KEY: None}
    return {
        USER_NOTIFICATIONS_COUNT_KEY: (
            Notification.objects.filter(
                to_account=request.user.account,
                read=False,
            ).count()
        )
    }
