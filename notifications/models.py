from typing import Optional, List

from django.db import models
from django.db.models import QuerySet

from accounts.models import Account
from catsordogs.utils.models import PrintableModelMixin
from polls.models import Comment


class NoAssociatedInfoError(Exception):
    pass


class NotificationQuerySet(QuerySet):
    def prefetch_info(self) -> 'NotificationQuerySet':
        return self.select_related(*Notification.get_select_related_fields())


class Notification(PrintableModelMixin, models.Model):
    class Meta:
        db_table = 'accounts_notification'

    to_account = models.ForeignKey(
        Account, related_name='notifications', on_delete=models.CASCADE
    )
    read = models.BooleanField(default=False)

    objects = NotificationQuerySet.as_manager()

    @staticmethod
    def get_select_related_fields() -> List[str]:
        return [
            info_type.reverse_name + '__' + related_field
            for info_type in NotificationInfo.__subclasses__()
            for related_field in info_type.select_related_fields
        ]

    @property
    def info(self) -> 'NotificationInfo':
        info_candidates = [
            info_type
            for info_type in NotificationInfo.__subclasses__()
            if getattr(self, info_type.reverse_name) is not None
        ]
        if len(info_candidates) == 0:
            raise NoAssociatedInfoError()
        if len(info_candidates) > 1:
            raise ValueError(
                f'Notifications {self} has more than 1 related info: '
                f'{info_candidates}'
            )
        return getattr(self, info_candidates[0].reverse_name)


class NotificationInfo(models.Model):
    reverse_name: str
    select_related_fields: List[str]

    class Meta:
        abstract = True

    @property
    def related_url(self) -> Optional[str]:
        raise NotImplementedError()

    @property
    def content(self) -> str:
        raise NotImplementedError()


class NotificationInfoPollNewComment(PrintableModelMixin, NotificationInfo):
    class Meta:
        db_table = 'accounts_notificationinfopollnewcomment'

    notification = models.OneToOneField(
        Notification,
        on_delete=models.CASCADE,
        related_name='info_poll_new_comment',
    )
    comment = models.ForeignKey(Comment, on_delete=models.SET_NULL, null=True)

    reverse_name = 'info_poll_new_comment'
    select_related_fields = [
        'comment',
        'comment__author__user',
        'comment__poll',
    ]

    @property
    def related_url(self) -> Optional[str]:
        if self.comment:
            return (
                self.comment.poll.url_for_view + f'#comment_{self.comment.pk}'
            )
        else:
            return None

    @property
    def content(self) -> str:
        if self.comment:
            return (
                f'User {self.comment.author.user.username} posted a comment '
                f'to your poll "{self.comment.poll.title}"'
            )
        else:
            return 'User posted a comment, but comment is deleted'
