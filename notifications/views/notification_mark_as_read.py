from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.decorators.http import require_POST

from notifications import use_cases


@login_required
@require_POST
def mark_notifications_as_read(request: HttpRequest) -> HttpResponse:
    account_pk = request.user.account.pk
    use_cases.mark_notifications_as_read(of_account_pk=account_pk)
    return redirect(reverse('accounts:me-profile'))
