from accounts.models import Account
from notifications import services as notifications_services


def mark_notifications_as_read(of_account_pk: int) -> None:
    account = Account.objects.get(pk=of_account_pk)
    notifications_services.mark_all_notifications_as_read(of_account=account)
