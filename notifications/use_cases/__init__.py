from .mark_notifications_as_read import mark_notifications_as_read


__all__ = [
    'mark_notifications_as_read',
]
