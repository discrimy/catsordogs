from django.contrib.auth.models import User
from django.db import models
from django.db.models import QuerySet
from django.db.models.signals import post_save
from django.dispatch import receiver


class AccountQuerySet(QuerySet):
    def get_by_username(self, username: str) -> 'Account':
        return self.get(user__username=username)


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    objects = AccountQuerySet.as_manager()

    def __str__(self):
        return f'{self.id}# {self.user.username}'


@receiver(post_save, sender=User)
def create_user_account(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)
