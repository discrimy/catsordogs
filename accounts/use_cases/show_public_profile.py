from dataclasses import dataclass
from typing import List


from accounts.models import Account
from polls.models import Poll


@dataclass(frozen=True)
class PollSchema:
    title: str
    url_for_view: str


@dataclass(frozen=True)
class PublicProfileSchema:
    username: str
    polls: List[PollSchema]

    @classmethod
    def from_account(
        cls, account: Account, public_polls: List[Poll]
    ) -> 'PublicProfileSchema':
        return PublicProfileSchema(
            username=account.user.username,
            polls=[
                PollSchema(
                    title=poll.title,
                    url_for_view=poll.url_for_view,
                )
                for poll in public_polls
            ],
        )


def show_public_profile(
    of_account_username: str,
) -> PublicProfileSchema:
    account = Account.objects.get_by_username(of_account_username)
    public_polls = Poll.objects.public_only().filter(author=account)
    return PublicProfileSchema.from_account(account, public_polls=public_polls)
