from dataclasses import dataclass
from typing import List, Optional

from accounts.models import Account
from notifications.models import Notification
from polls.models import Poll


@dataclass(frozen=True)
class PollSchema:
    title: str
    url_for_view: str
    is_public: bool


@dataclass(frozen=True)
class NotificationSchema:
    content: str
    related_url: Optional[str]


@dataclass(frozen=True)
class PrivateProfileSchema:
    username: str
    all_polls: List[PollSchema]
    unread_notifications: List[NotificationSchema]

    @classmethod
    def from_account(
        cls,
        account: Account,
        all_polls: List[Poll],
        unread_notifications: List[Notification],
    ) -> 'PrivateProfileSchema':
        return PrivateProfileSchema(
            username=account.user.username,
            all_polls=[
                PollSchema(
                    title=poll.title,
                    url_for_view=poll.url_for_view,
                    is_public=poll.is_public,
                )
                for poll in all_polls
            ],
            unread_notifications=[
                NotificationSchema(
                    content=notification.info.content,
                    related_url=notification.info.related_url,
                )
                for notification in unread_notifications
            ],
        )


def show_private_profile(
    of_account_username: str,
) -> PrivateProfileSchema:
    account = Account.objects.select_related('user').get_by_username(
        of_account_username
    )
    public_polls = Poll.objects.filter(author=account)
    unread_notifications = Notification.objects.prefetch_info().filter(
        to_account=account,
        read=False,
    )
    return PrivateProfileSchema.from_account(
        account,
        all_polls=public_polls,
        unread_notifications=unread_notifications,
    )
