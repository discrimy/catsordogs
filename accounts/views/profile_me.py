from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET

from accounts import use_cases


@login_required
@require_GET
def show_private_profile(request: HttpRequest) -> HttpResponse:
    username = request.user.username
    profile_schema = use_cases.show_private_profile(
        of_account_username=username
    )
    return render(
        request,
        'accounts/me_profile.html',
        context=dict(account=profile_schema),
    )
