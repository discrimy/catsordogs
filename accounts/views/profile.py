from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET

from accounts import use_cases
from catsordogs.errors import NotFoundError
from catsordogs.utils.errors import wrap_exception


@require_GET
@wrap_exception(NotFoundError, Http404)
def show_public_profile(request: HttpRequest, username: str) -> HttpResponse:
    profile_schema = use_cases.show_public_profile(
        of_account_username=username
    )
    return render(
        request, 'accounts/profile.html', context=dict(account=profile_schema)
    )
