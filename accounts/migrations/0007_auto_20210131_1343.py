# Generated by Django 3.1.4 on 2021-01-31 10:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0022_auto_20201228_1907'),
        ('accounts', '0006_notification_related_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='content',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='related_url',
        ),
        migrations.CreateModel(
            name='NotificationInfoPollNewComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='polls.comment')),
                ('notification', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='info_poll_new_comment', to='accounts.notification')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
