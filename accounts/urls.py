from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from accounts.views import (
    show_private_profile,
    show_public_profile,
)

app_name = 'accounts'
urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('me/', show_private_profile, name='me-profile'),
    path('users/<str:username>/', show_public_profile, name='user-profile'),
]
